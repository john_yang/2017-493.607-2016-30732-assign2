import argparse
import re
import operator

parser = argparse.ArgumentParser(description='run ---> python main.py Aladdin.txt')
# parser.add_argument('--logdir', dest='logdir', action='store', default='/home/timelighter/save/osim_rl')

parser.add_argument("file")
arg = parser.parse_args()
# spec_chars = ['\t', '\r', '\n', ' ', '"', ':', ',', ';', '`', "(", ")", "!", "?", "{", "}", ".", '\\']
filepath = arg.file

def count_words(filepath) :
    word_count = {}
    # sentence to words
    with open(filepath,'r') as f:
        for line in f:
            for word in line.split(" "):
                new_word = re.sub('\s', '', word)
                ## check for symbols and discard them if any,
                new_word = re.sub(r"""["?,$!]|'(?!(?<! ')[ts])""", "", word)
                new_word = re.sub(r"\n|\r|\.|\)|\(|:|-| ",  "", new_word)

                ## uppercase to lowercase
                # print new_word
                new_word = re.sub(r'^.*([A-Z].*)$', lambda m: m.group(0).lower(), new_word)

                if new_word == '' : # Dealing with bugs..
                    continue

                ## append to dictionary with counts
                if new_word in word_count :
                    word_count[new_word] += 1
                else :
                    word_count[new_word] = 1
    return word_count



def main(filepath) :
    word_count = count_words(filepath)
    sorted_words = sorted(word_count.items(), key=operator.itemgetter(1), reverse=True)
    for i in sorted_words :
        print i[0], ' : ', i[1]


if __name__ == "__main__":
    # filepath = arg.file
    main(filepath)